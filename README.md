# Protótipo de Interface Web para servidor IRC utilizando Socket.io

## Instalação:

1) Instale o Node.js e NPM.

2) Para baixar e instalar:
```
# baixar:
git clone https://gitlab.com/sd1-ec-2016-2/p3-g3.git

# entrar no diretório
cd web-irc-socketio

# instalar as dependências
apt-get install libicu-dev

# instalar módulos dependentes
npm install express cookie-parser body-parser socket.io socket.io-cookie irc

# para executar:
nodejs app.js

# abra a url http://localhost:8080 no  navegador 
```


