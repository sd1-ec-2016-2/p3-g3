// configuracao do express
var app = require('express')();  // módulo express
var server = require('http').Server(app);
var cookieParser = require('cookie-parser');  // processa cookies
app.use(cookieParser());
var bodyParser = require('body-parser');  // processa corpo de requests
app.use(bodyParser.urlencoded( { extended: true } ));

// configuracao do socket.io
var socketio = require('socket.io')(server);
var socketio_cookieParser = require('socket.io-cookie');
socketio.use(socketio_cookieParser);
server.listen(8080, function() {
  console.log('Listening at: http://localhost:8080');
});
socketio.listen(server);

var irc = require('irc');

var fs = require('fs');
var path = require('path');	// módulo usado para lidar com caminhos

var proxies = {}; // mapa de proxys
var proxy_id = 1;

var Proxy = function(servidor, nick, canal) {
  this.id = proxy_id++;
  this.servidor = servidor;
  this.nick = nick;
  this.canal = canal;
  this.socket = null;
  this.url = null;
  var firstConnection = true;
  this.irc_client = new irc.Client(
    servidor,
    nick,
    {channels: [canal],});
    proxies[this.id] = this;

    socketio.on('connection', function (socket) {
      // o proxy_id pode ser obtido a partir do websocket
      var proxy_id = socket.request.headers.cookie.id;
      // executa somente se o socket ainda não foi criado
      if ( ! proxies[proxy_id].socket ) {
        proxies[proxy_id].socket = socket;
        socket.on('message', function (msg) {
          if (msg.substring(0,1) == '/') {
            var cmd = "";
            var par = "";
            for(var i = 1; i < msg.length; i++){
              if(msg[i] != " ")
              cmd += msg[i];
              else {
                par = msg.substr(i+1);
                break;
              }
            }
            var proxy_id = this.request.headers.cookie.id;
            proxies[proxy_id].processar_comando(cmd, par, proxies[proxy_id].socket);
          }
          else {
            var proxy_id = this.request.headers.cookie.id;
            // console.log('Message Received: '+msg+' de proxy_id: '+proxy_id);
            var client = proxies[proxy_id].irc_client;
            client.say(client.opt.channels[0], msg);
          }
        });
      }
    });
    this.processar_comando = function(cmd, par, socket) {
      switch (cmd) {

        case "lusers":
        var canal = this.irc_client.opt.channels[0];
        this.irc_client.send('names', canal);
        break;

        case 'whois':
        if(par == ""){
          console.log("[WHOIS Reply] error: insira um parametro.");
          socket.emit('message', {"timestamp":Date.now(),
          "nick":'[WHOIS Reply] error',
          "msg":"insira um parametro."});
        }
        else
        this.irc_client.send('whois', par);
        break;

        case 'pm':
        if (par == "")
        {
          console.log("[PM Reply] erro: insira o nick e a mensagem como parametros.");
          socket.emit('message', {"timestamp":Date.now(),
          "nick":'[PM Reply] error',
          "msg":"insira o nick e a mensagem como parametros."});
        }
        else {
          var toNick = "";
          var message = "";
          for(var i = 0; i < par.length; i++){
            if(par[i] != " ")
            toNick += par[i];
            else {
              message = par.substr(i+1);
              break;
            }
          }
          if (message == "") {
            console.log("[PM Reply] erro: insira a mensagem como parametro apos o nick de destino.");
            socket.emit('message', {"timestamp":Date.now(),
            "nick":'[PM Reply] error',
            "msg":"insira a mensagem como parametro apos o nick de destino."});
          }
          else {
            this.irc_client.say(toNick, message);
            socket.emit('message', {"timestamp":Date.now(),
            "nick":'[to ' + toNick + ']',
            "msg":message});
          }
        }
        break;

        case 'nick':
        if(par == "")
        {
          console.log("[Nick] error: insira um nick");
          socket.emit('message', {"timestamp":Date.now(),
          "nick":'[Nick] error',
          "msg":"insira um nick."});
        }
        else
        {
          this.irc_client.send('nick', par);
        }
        break;

        case 'join':
        if(par == "")
        {
          socket.emit('message', {"timestamp":Date.now(),
          "nick":'[Join] error',
          "msg":"insira um canal."});
          console.log("error: insira um canal");
        }
        else
        {
          this.irc_client.send('join', par);
        }
        break;

        case 'away':
        this.irc_client.send('away', par);
        break;


        case 'notice':
        // quebrando o parâmetro entre nick e mensagem
        var nick = "";
        var msg = "";
        for(var i = 1; i < par.length; i++){
          if(par[i] != " ")
          nick += par[i];
          else {
            msg = par.substr(i+1);
            break;
          }
        }
        this.irc_client.send('notice', nick, msg);
        break;

        case 'kick':
        if(par == ""){
          console.log("[KICK Reply] error: insira um parametro.");
          socket.emit('message', {"timestamp":Date.now(),
          "nick":'[WHOIS Reply] error',
          "msg":"insira um parametro."});
        }
          this.irc_client.send('kick', par);
        break;

        case 'kill':
        var nick = "";
        var reason = "";
        for(var i = 1; i < par.length; i++){
          if(par[i] != " ")
          nick += par[i];
          else {
            reason = par.substr(i+1);
            break;
          }
        }
        this.irc_client.send('kill', nick, reason);
        break;

        case 'quit':
        this.irc_client.disconnect();
        break;


      }
    };
    // inclui a propriedade proxy_id no irc_client
    this.irc_client.proxy_id = this.id;

    this.irc_client.addListener('message'+canal, function (from, message) 	 {
      var socket = proxies[this.proxy_id].socket;
      socket.emit('message', {"timestamp":Date.now(),
      "nick":from,
      "msg":message} );
    });
    this.irc_client.addListener('whois', function(info) {
      if (firstConnection == true)
      {
        // Evitar que as informações de whois do usuário sejam fornecidas ao se conectar ao servidor.
        firstConnection = false;
      }
      else {
        var socket = proxies[this.proxy_id].socket;
        console.log('whois results: ', info);
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Nick',
        "msg":info.nick});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] User',
        "msg":info.user});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Host',
        "msg":info.host});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Real Name',
        "msg":info.realname});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Channels',
        "msg":JSON.stringify(info.channels)});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Server',
        "msg":info.server});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Server Info',
        "msg":info.serverinfo});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Idle',
        "msg":info.idle});
        if (info.away)
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply] Away',
        "msg":info.away});
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[WHOIS Reply]',
        "msg":'End of Whois.'});
      }

    });

    this.irc_client.addListener('names', function(channel, nicks) {
      var lista = channel+': '+JSON.stringify(nicks);
      console.log('listagem de nicks do canal:'+lista);
      var socket = proxies[this.proxy_id].socket;
      socket.emit('message', {"timestamp":Date.now(),
      "nick":'[SERVER]',
      "msg":"*** Listagem de nicks do canal "+lista});
    });
    this.irc_client.addListener('error', function(message) {
      console.log('error: ', message);
    });
    this.irc_client.addListener('away', function(message) {
      var socket = proxies[this.proxy_id].socket;
      console.log('[Away]: ', message);
      socket.emit('message', {"timestamp":Date.now(),
      "nick":'[Away]',
      "msg":message});
    });
    this.irc_client.addListener('pm', function(nick, message) {
      var socket = proxies[this.proxy_id].socket;
      console.log('Got private message from %s: %s', nick, message);
      socket.emit('message', {"timestamp":Date.now(),
      "nick":'[From ' + nick + ']',
      "msg":message});
    });
    this.irc_client.addListener('nick', function(oldnick, newnick, channels){
      var socket = proxies[this.proxy_id].socket;
      console.log('nick: '+oldnick+' has changed to '+newnick);
      socket.emit('message', {"timestamp":Date.now(),
      "nick":'[Nick]',
      "msg":'nick: '+oldnick+' has changed to '+newnick});
    });
    this.irc_client.addListener('registered', function(message) {
      var socket = proxies[this.proxy_id].socket;
      console.log('conectado:'+JSON.stringify(message));
      var socket = proxies[this.proxy_id].socket;
      this.url = 'irc://'+this.opt.nick+'@'+
      this.opt.server+'/';
      socket.emit('message', {"timestamp":Date.now(),
      "nick":'[SERVER]',
      "msg":"*** Conectado em "+this.url});
    });
    this.irc_client.addListener('join', function(message) {
      var socket = proxies[this.proxy_id].socket;
      socket.emit('message', {"timestamp":Date.now(),
      "nick":'[SERVER]',
      "msg":'*** Conectado ao canal ' + message});
    });
    this.irc_client.addListener('notice', function (nick, to, text, message) {
      var socket = proxies[this.proxy_id].socket;
      console.log("nick: " + nick + " /to: " + to + " /message: "+ message);
      if (nick == null)
      {
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[SERVER]',
        "msg":text});
      }
      else if(to == this.irc_client.nick) {
        socket.emit('message', {"timestamp":Date.now(),
        "nick":'[From ' + nick + ']',
        "msg":message});
      }
    });
    this.irc_client.addListener('kill', function(nick, reason, channels, message) {
      console.log("nick: " + nick + " has been killed reason: " + reason);
      socket.emit('message', {"timestamp":Date.now(),
      "nick": nick + " has been killed",
      "reason":reason});
    });

    this.irc_client.addListener('kick', function(channel, nick, by, reason, message){
      consele.log(nick+ 'was kicked by '+by);
      socket.emit('message', {"timestamp":Date.now(),
      "nick":"[Channel]", "msg":"KICK "+message+" from "+by+ " to remove"+ nick+ "from" +channel});
    });

  /*  this.irc_client.addListener('raw', function(message) {
      console.log('\n[RAW MESSAGE]: ' + JSON.stringify(message) + '\n');
    });*/
  }


  app.get('/', function (req, res) {
    if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
      var proxy =	new Proxy(
        req.cookies.servidor,
        req.cookies.nick,
        req.cookies.canal);
        res.cookie('id', proxy.id);
        res.sendFile(path.join(__dirname, '/index.html'));
      }
      else {
        res.sendFile(path.join(__dirname, '/login.html'));
      }
    });

    app.post('/login', function (req, res) {
      res.cookie('nick', req.body.nome);
      res.cookie('canal', req.body.canal);
      res.cookie('servidor', req.body.servidor);
      res.redirect('/');
    });
